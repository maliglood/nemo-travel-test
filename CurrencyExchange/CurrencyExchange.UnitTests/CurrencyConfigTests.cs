﻿using NUnit.Framework;

namespace CurrencyExchange.UnitTests
{
    [TestFixture]
    public class CurrencyConfigTests
    {
        [Test]
        public void CurrencyConfig_CurrenciesList_IsNotEmpty()
        {
            CollectionAssert.IsNotEmpty(CurrencyConfig.CurrenciesList);
        }

        [Test]
        public void CurrencyConfig_CurrenciesList_ContainsCurrencyMeasur()
        {
            CollectionAssert.Contains(CurrencyConfig.CurrenciesList.Keys, CurrencyConfig.CurrencyMeasure);
        }
    }
}
