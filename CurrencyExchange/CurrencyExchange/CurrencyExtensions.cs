﻿namespace CurrencyExchange
{
    public static class CurrencyExtensions
    {
        /// <summary>
        /// Sums two currency exemplars and returns new result in the first units.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Currency Sum(this Currency a, Currency b)
        {
            var bInaUnit = b.Convert(a.Unit);
            var resultValueInaUnit = a.Value + bInaUnit.Value;
            var resultInaUnit = new Currency(a.Unit, resultValueInaUnit);
            return resultInaUnit;
        }
        /// <summary>
        /// Substracts the second currency from the first.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Currency Substract(this Currency a, Currency b)
        {
            var bInaUnit = b.Convert(a.Unit);
            var resultValueInaUnit = a.Value - bInaUnit.Value;
            var resultInaUnit = new Currency(a.Unit, resultValueInaUnit);
            return resultInaUnit;
        }
    }
}
