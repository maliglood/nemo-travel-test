﻿using System.Linq;

namespace CurrencyExchange
{
    /// <summary>
    /// Result of calculatings of currencies.
    /// </summary>
    public class CurrencyResult
    {
        private readonly Currency[] _currencies;

        public Currency[] Currencies => _currencies;

        public CurrencyResult(Currency a)
        {
            _currencies = new Currency[1] { a };
        }

        public CurrencyResult(Currency a, Currency b)
        {
            _currencies = new Currency[2] { a, b };
        }

        public Currency ChooseCurrency(string unit)
        {
            return _currencies.First(currency => currency.Unit == unit.ToLowerInvariant());
        }

        public Currency First()
        {
            return _currencies.First();
        }
    }
}
