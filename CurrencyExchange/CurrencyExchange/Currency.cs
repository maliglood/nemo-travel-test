﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CurrencyExchange
{
    public struct Currency
    {
        public readonly string _unit;

        public double Value { get; set; }

        public string Unit => _unit;

        public Currency(string unit, double value)
        {
            if (!CurrencyConfig.CurrenciesList.Keys.Contains(unit))
            {
                throw new Exception($"The transferred currency ({unit}) does not exist in the list of currencies.");
            }

            _unit = unit;
            Value = value;
        }

        /// <summary>
        /// Returns exemplar with new unit and value.
        /// </summary>
        /// <param name="goalUnit"></param>
        /// <returns></returns>
        public Currency Convert(string goalUnit)
        {
            if (!CurrencyConfig.CurrenciesList.Keys.Contains(goalUnit))
            {
                throw new Exception($"The transferred currency ({goalUnit}) does not exist in the list of currencies.");
            }

            var currentRate = CurrencyConfig.CurrenciesList[_unit];
            var goalRate = CurrencyConfig.CurrenciesList[goalUnit];
            var newValue = Value / currentRate * goalRate;

            return new Currency(goalUnit, newValue);
        }

        public override bool Equals(object obj)
        {
            return obj is Currency currency && Unit == currency.Unit && Value == currency.Value;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Unit, Value);
        }

        public static CurrencyResult operator +(Currency a, Currency b)
        {
            if (a.Unit == b.Unit)
            {
                var newValue = a.Value + b.Value;
                var newCurrency = new Currency(a.Unit, newValue);
                return new CurrencyResult(newCurrency);
            }
            else
            {
                var resultInaUnit = a.Sum(b);
                var resultInbUnit = b.Sum(a);
                return  new CurrencyResult(resultInaUnit, resultInbUnit);
            }
        }

        public static CurrencyResult operator -(Currency a, Currency b)
        {
            if (a.Unit == b.Unit)
            {
                var newValue = a.Value - b.Value;
                var newCurrency = new Currency(a.Unit, newValue);
                return new CurrencyResult(newCurrency);
            }
            else
            {
                var resultInaUnit = a.Substract(b);
                var resultInbUnit = b.Substract(a);
                return new CurrencyResult(resultInaUnit, resultInbUnit);
            }
        }

        public static Currency operator +(Currency a, int b)
        {
            var newValue = a.Value + b;
            return new Currency(a.Unit, newValue);
        }

        public static Currency operator +(Currency a, long b)
        {
            var newValue = a.Value + b;
            return new Currency(a.Unit, newValue);
        }

        public static Currency operator +(Currency a, double b)
        {
            var newValue = a.Value + b;
            return new Currency(a.Unit, newValue);
        }

        public static Currency operator +(Currency a, float b)
        {
            var newValue = a.Value + b;
            return new Currency(a.Unit, newValue);
        }

        public static Currency operator -(Currency a, int b)
        {
            var newValue = a.Value - b;
            return new Currency(a.Unit, newValue);
        }

        public static Currency operator -(Currency a, long b)
        {
            var newValue = a.Value - b;
            return new Currency(a.Unit, newValue);
        }

        public static Currency operator -(Currency a, double b)
        {
            var newValue = a.Value - b;
            return new Currency(a.Unit, newValue);
        }

        public static Currency operator -(Currency a, float b)
        {
            var newValue = a.Value - b;
            return new Currency(a.Unit, newValue);
        }
    }
}
