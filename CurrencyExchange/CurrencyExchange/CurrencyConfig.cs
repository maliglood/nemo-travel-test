﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CurrencyExchange
{
    public static class CurrencyConfig
    {
        /// <summary>
        /// Default reference unit of currency.
        /// </summary>
        private const string DefaultCurrencyMeasure = "ruble";

        /// <summary>
        /// All default currencies.
        /// </summary>
        private static readonly Dictionary<string, double> _defaultCurrenciesList = new Dictionary<string, double>
        {
            { DefaultCurrencyMeasure, 1 },
            { "dollar", 59.41 },
            { "euro", 60.06 }
        };

        /// <summary>
        /// Reference unit of currency.
        /// </summary>
        public static string CurrencyMeasure { get; private set; } = DefaultCurrencyMeasure;

        /// <summary>
        /// All currencies.
        /// </summary>
        public static Dictionary<string, double> CurrenciesList { get; private set; } = _defaultCurrenciesList;

        /// <summary>
        /// Configures custom exchange rates.
        /// </summary>
        /// <param name="newCurrenciesList"> New list of currencies. </param>
        /// <param name="currencyMeasure"> New currency measure. </param>
        public static void ConfigureCurrencies(Dictionary<string, double> newCurrenciesList, string currencyMeasure)
        {
            if (!newCurrenciesList.Keys.Contains(currencyMeasure))
            {
                throw new Exception($"List of currencies does not contain the currency measure: currencyMeasure.");
            }

            CurrenciesList = newCurrenciesList.ToDictionary(currency => currency.Key.ToLowerInvariant(), currency => currency.Value);
            CurrencyMeasure = currencyMeasure.ToLowerInvariant();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currencyMeasure"> Name of currency. </param>
        /// <param name="currencyRates"> Rate of currency. </param>
        public static void AddCurrency(string currencyMeasure, double currencyRates)
        {
            var currencyMeasureLower = currencyMeasure.ToLowerInvariant();

            if (CurrenciesList.Keys.Contains(currencyMeasureLower))
            {
                throw new Exception($"The transferred currency ({ currencyMeasureLower }) is already exist.");
            }

            CurrenciesList.Add(currencyMeasureLower, currencyRates);
        }

        /// <summary>
        /// Sets new currency measure and recalculates exchange rates for it (not implemented!).
        /// </summary>
        /// <param name="currencyMeasure"> New currency measure. </param>
        public static void SetCurrencyMeasure(string currencyMeasure)
        {
            throw new NotImplementedException();
            //var currencyMeasureLower = currencyMeasure.ToLowerInvariant();
            //if (!CurrenciesList.Keys.Contains(currencyMeasureLower))
            //{
            //    throw new Exception($"The transferred currency ({currencyMeasureLower}) is not contained in the list of currencies.");
            //}
            // TODO: Calculating of exchange rates for new currency measure
        }
    }
}
